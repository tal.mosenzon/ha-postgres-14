#!/bin/bash

if [ "${name}" = 'pgsql1' ]; then
    echo ${name} 
    sed "s/NAME/$name/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/my-ip/$patroni1/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/patroni2/$patroni2/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/patroni3/$patroni3/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml

    patroni /etc/patroni/config.yml
fi


if [ "${name}" = 'pgsql2' ]; then
    echo ${name} 
    sed "s/NAME/$name/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/my-ip/$patroni1/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/patroni2/$patroni2/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/patroni3/$patroni3/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    patroni /etc/patroni/config.yml
fi

if [ "${name}" = 'pgsql3' ]; then
    echo ${name}
    sed "s/NAME/$name/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/my-ip/$patroni1/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/patroni2/$patroni2/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    sed "s/patroni3/$patroni3/g" /etc/patroni/config.yml > /tmp/my.cnf && cat /tmp/my.cnf > /etc/patroni/config.yml
    patroni /etc/patroni/config.yml
fi